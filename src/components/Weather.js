import React from 'react'

 const Weather = (props) => {
  return (
    <div className="container">
        {
            props.cityc && <h2 className="mt-3">{props.cityc}</h2>
        }
        <ul className="mt-4"> 
            {
                props.currentTemp && <li>Current tempreature : {props.currentTemp} C</li>
            }
            {
                props.maxTemp && <li>Max tempreature : {props.maxTemp} C</li>
            }
            {
                props.minTemp && <li>Min tempreature : {props.minTemp} C</li>
            }
            {
                props.humidity && <li>Humidity : {props.humidity}</li>
            }
            {
                props.country && <li>Country : {props.country}</li>
            }
            {
                props.localtime && <li>Localtime : {props.localtime}</li>
            }
            {
                props.error && <li>{props.error}</li>
            }
        </ul>
    </div>  
  )
}

export default Weather;