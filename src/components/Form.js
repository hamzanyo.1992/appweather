import React from 'react';


const Form = (props) => {
    return(
        <div className="col-md-12">
            <form onSubmit={props.getWeather}>
                <input type="text" placeholder="...City" name="city" />
                <button>GO</button>
            </form>
        </div>
    )
}

export default Form;