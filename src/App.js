import React, { Component } from 'react';
import Form from './components/Form';
import Weather from './components/Weather';
import './App.css';

const keyapp="e9b31637523644a5984103104191501";
// https://api.apixu.com/v1/forecast.json?key=e9b31637523644a5984103104191501&q=rabat
class App extends Component {

  state={
    cityc:'',
    currentTemp:'',
    maxTemp:'',
    minTemp:'',
    humidity:'',
    country:'',
    localtime:'',
    cdate:'',
    error:''
  }

  getWeather = async (e) =>{
    e.preventDefault();
    const city = e.target.elements.city.value;
    try {
    const apidata = await fetch(`https://api.apixu.com/v1/forecast.json?key=${keyapp}&q=${city}`)
    const data = await apidata.json();
      if(city){
        this.setState({
          cityc:data.location.name,
          currentTemp:data.current.temp_c,
          maxTemp:data.forecast.forecastday[0].day.maxtemp_c,
          minTemp:data.forecast.forecastday[0].day.mintemp_c,
          humidity:data.current.humidity,
          country:data.location.country,
          localtime:data.location.localtime,
          cdate:data.forecast.forecastday[0].date,
          error:''
        })
      }else{
        this.setState({
          cityc:'',
          currentTemp:'',
          maxTemp:'',
          minTemp:'',
          humidity:'',
          country:'',
          localtime:'',
          cdate:'',
          error:'Please Enter Data'
        })
      }
    }
    catch(err){
      console.log('Error: ', err)
      this.setState({
        cityc:'',
        currentTemp:'',
        maxTemp:'',
        minTemp:'',
        humidity:'',
        country:'',
        localtime:'',
        cdate:'',
        error:'is not a city'
      })
    }
  }

  render() {
    return (
      <React.Fragment>
        <div className="container">
          <h1 className="mb-4 mt-2">Weather app</h1>
          <Form getWeather={this.getWeather}/>
          <Weather  
            cityc={this.state.cityc}
            currentTemp={this.state.currentTemp}
            maxTemp={this.state.maxTemp}
            minTemp={this.state.minTemp}
            humidity={this.state.humidity}
            country={this.state.country}
            localtime={this.state.localtime}
            cdate={this.state.cdate}
            error={this.state.error}
          />
        </div>
      </React.Fragment>
    );
  }
}

export default App;
